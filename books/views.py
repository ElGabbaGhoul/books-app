from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from .models import Author, Book, Magazine, Genre
from .forms import BookForm, MagazineForm


# -----------------------------------------------------------
# Book Views
# -----------------------------------------------------------

def show_books(request):
    books = Book.objects.all()
    context = {
        'books': books
        
    }
    return render(request, 'book/list.html', context)

def create_view(request):
    context = {}
    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect ('show_books')
    
    context['form']= form
    return render(request, 'book/create.html', context)

def detail_view(request, pk):
    context = {
        'book': Book.objects.get(pk=pk) if Book else None,
        'author': Author.objects.get(pk=pk) if Author else None,
    }
    return render(request, 'book/detail.html', context)

def delete_book(request, pk):
    context = {'book' : Book.objects.all()}
    obj = get_object_or_404(Book, pk = pk)
    if request.method == 'POST':
        obj.delete()
        return redirect('books/')
    return render(request,'book/delete.html', context)

def update_book(request, pk):
    book = get_object_or_404(Book, pk=pk)

    form = BookForm(request.POST or None, instance=book)
    if form.is_valid():
        form.save()
        return redirect ('show_books')
        
    context = {'form': form}
    return render(request, 'book/update.html', context)


# -----------------------------------------------------------
# Magazine Views
# -----------------------------------------------------------

def show_magazines(request):
    magazines = Magazine.objects.all()
    context = {
        'magazines': magazines
    }
    return render(request, 'magazine/list.html', context)

def create_magazine(request):
    context = {}
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect ('show_magazines')

    context['form'] = form
    return render(request, 'magazine/create.html', context)

def mag_detail(request, pk):
    context = {
        'magazine': Magazine.objects.get(pk=pk) if Magazine else None,
    }
    return render(request, 'magazine/detail.html', context)

def delete_mag(request, pk):
    context = {'book' : Magazine.objects.all()}
    obj = get_object_or_404(Magazine, pk = pk)
    if request.method == 'POST':
        obj.delete()
        return redirect('show_magazines')
    if request.method == None:
        return redirect('show_magazines')
    return render(request,'magazine/delete.html', context)

def update_mag(request, pk):
    magazine = get_object_or_404(Magazine, pk=pk)

    form = MagazineForm(request.POST or None, instance=magazine)
    if form.is_valid():
        form.save()
        return redirect ('show_magazines')
        
    context = {'form': form}
    return render(request, 'magazine/update.html', context)

def genre_detail(request, pk):
    context = {
        'genre': Genre.objects.get(pk=pk) if Genre else None,
    }
    return render(request, 'magazine/genre_details.html', context)





# login required decorator
@login_required(login_url='/accounts/login/')
def list_reviews(request):
    # how do I get user who made request? - 
    book_reviews_by_this_user = request.user.reviews_by_this_user.all()
    print(request.user)
    # how do I get all model instances made by that user? - 
    context = {
        'reviews_queryset': book_reviews_by_this_user
    } 

    return render (request, 'book/review_list.html', context)