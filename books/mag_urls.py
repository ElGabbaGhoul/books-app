from django.urls import path
from books.views import genre_detail, create_magazine, delete_mag, show_magazines, mag_detail, update_mag

# MAGAZINE URLS

urlpatterns = [
    path('', show_magazines, name='show_magazines'),
    path('create/', create_magazine, name='create_magazine'),
    path('<int:pk>/', mag_detail, name='magazine_details'),
    path('<int:pk>/delete/', delete_mag, name ='delete_magazine'),
    path('<int:pk>/edit/', update_mag, name='edit_magazines'),
    path('genre/<int:pk>/', genre_detail, name='genre_details'),
]