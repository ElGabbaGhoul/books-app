from django.urls import path
from books.views import (
    delete_book, 
    show_books, 
    create_view, 
    detail_view, 
    delete_book, 
    update_book,
    list_reviews,

)

# BOOKS URLS

urlpatterns = [
    path('', show_books, name="show_books"),
    path('create/', create_view, name='create_book'),
    path('reviews/', list_reviews, name="list_reviews"),
    path('<int:pk>/', detail_view, name='book_details'),
    path('<int:pk>/edit/', update_book, name="edit_books"),
    path('<int:pk>/delete/', delete_book, name ='delete_book'),
    
]