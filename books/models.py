from django.db import models
from django.contrib.auth.models import User


class Book(models.Model):
    title = models.CharField(max_length=200, unique=True)
    author = models.ManyToManyField('Author', related_name='books')
    pages = models.IntegerField(null=True)
    isbn = models.IntegerField(null=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)
    year_published = models.SmallIntegerField(null=True)

    def __str__(self):
        return self.title + " by " + str(self.author.first())


class Author(models.Model):
    name = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.name


class BookReview(models.Model):
    reviewer = models.ForeignKey(User, related_name="reviews_by_this_user", on_delete=models.CASCADE, null=True)
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return self.reviewer


class Magazine(models.Model):
    class CyclePeriod(models.TextChoices):
        WEEKLY = 'Weekly', ('Weekly')
        MONTHLY = 'Monthly', ('Monthly')
        QUARTERLY = 'Quarterly', ('Quarterly')
        YEARLY = 'Yearly', ('Yearly')

    title = models.CharField(max_length=200, unique=True)
    cycle = models.CharField(
        max_length=10,
        choices=CyclePeriod.choices,
        default=CyclePeriod.QUARTERLY,
    )
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    genre = models.ForeignKey("Genre", on_delete = models.CASCADE, related_name="magazines", null=True)  


    def __str__(self):
        return self.title 


class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name='issues', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField()
    date_publish = models.SmallIntegerField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True, blank=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    cover_image = models.URLField(null=True, blank=True)


    def __str__(self):
        return self.title


class Genre(models.Model):
    genre_name = models.CharField(max_length=200)
    mags = models.ManyToManyField(Magazine, related_name='genres')

    def __str__(self):
        return self.genre_name