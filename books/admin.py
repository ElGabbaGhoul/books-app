from django.contrib import admin
from .models import Author, Book, BookReview, Genre, Magazine, Issue

class AuthorAdmin(Author):
    pass
class BookAdmin(Book):
    pass
class BookReviewAdmin(BookReview):
    pass
class GenreAdmin(Genre):
    pass
class IssueAdmin(Issue):
    pass
class MagazineAdmin(Magazine):
    pass

admin.site.register(Author)
admin.site.register(Book)
admin.site.register(BookReview)
admin.site.register(Genre)
admin.site.register(Issue)
admin.site.register(Magazine)