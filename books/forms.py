from django import forms
from .models import Book, Magazine

class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        # fields = ["title", "author", "pages", "isbn", "image", "in_print", "year_published"]
        # alternatively
        exclude = []

class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine
        exclude = []